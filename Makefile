target/list: code/list.h code/list.cpp code/main.cpp | target
	g++ -fsanitize=address --std=c++17 code/list.cpp code/main.cpp -o target/list

target:
	mkdir target

clean:
	rm -rf target
