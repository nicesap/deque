#include "list.h"

#include <cstdio>
#include <cassert>

int main()
{
    Deque deque;

    deque.pushBack(1);
    deque.front() = 2;
    deque.pushFront(1);
    deque.pushBack(3);
    deque.pushBack(0);
    deque.back() = 4;

    bool f = deque->hasNext();

    assert(not f);
}
