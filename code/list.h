#pragma once

#include <cstddef>

class Deque final
{
private:
    class Node final
    {
    public:
        explicit Node(int value, Node* prev, Node* next);

        int& operator*();
        int operator*() const;

        void setNext(Node* next);
        void setPrev(Node* prev);

        Node* prev();
        Node* next();

        const Node* operator->() const;

        bool hasPrev() const;
        bool hasNext() const;

    private:
        int mValue;
        Node* mPrev;
        Node* mNext;
    };

public:
    explicit Deque();
    ~Deque();

    void pushBack(int value);
    void popBack();

    int& back();
    int back() const;

    void pushFront(int value);
    void popFront();

    int& front();
    int front() const;

    bool empty() const;
    size_t size() const;

    const Node* operator->() const;

private:
    size_t mSize;
    Node* mBack;
    Node* mFront;
};
