#include "list.h"

#include <cassert>
#include <cstdio>

Deque::Deque()
    : mSize{0}
    , mBack{nullptr}
    , mFront{nullptr}
{
}

Deque::~Deque()
{
    for (Node* node = mBack; node;)
    {
        Node* next = node->next();
        delete node;
        node = next;
    }
}

void Deque::pushBack(int value)
{
    if (empty())
    {
        Node* node = new Node{value, nullptr, nullptr};
        mBack = node;
        mFront = node;
    }
    else
    {
        assert(mFront && mBack);
        Node* newBack = new Node{value, nullptr, mBack};
        mBack->setPrev(newBack);
        mBack = newBack;
    }
    ++mSize;
}

void Deque::pushFront(int value)
{
    if (empty())
    {
        Node* node = new Node{value, nullptr, nullptr};
        mBack = node;
        mFront = node;
    }
    else
    {
        assert(mFront && mBack);
        Node* newFront = new Node{value, mFront, nullptr};
        mFront->setNext(newFront);
        mFront = newFront;
    }
    ++mSize;
}

int& Deque::back()
{
    assert(not empty());
    return **mBack;
}

int Deque::back() const
{
    assert(not empty());
    return **mBack;
}

int& Deque::front()
{
    assert(not empty());
    return **mFront;
}

int Deque::front() const
{
    assert(not empty());
    return **mFront;
}

bool Deque::empty() const
{
    return mSize == 0;
}

size_t Deque::size() const
{
    return mSize;
}

const Deque::Node* Deque::operator->() const
{
    assert(not empty());
    printf("Deque::operator->()\n");

    return mBack->operator->();
}

Deque::Node::Node(int value, Node* prev, Node* next)
    : mValue{value}
    , mPrev{prev}
    , mNext{next}
{
}

int& Deque::Node::operator*()
{
    return mValue;
}

int Deque::Node::operator*() const
{
    return mValue;
}

void Deque::Node::setNext(Node* next)
{
    mNext = next;
}

void Deque::Node::setPrev(Node* prev)
{
    mPrev = prev;
}

Deque::Node* Deque::Node::prev()
{
    return mPrev;
}

Deque::Node* Deque::Node::next()
{
    return mNext;
}

const Deque::Node* Deque::Node::operator->() const
{
    printf("Node::operator->() %d\n", mValue);

    if (hasNext())
    {
        return mNext->operator->();
    }

    return this;
}

bool Deque::Node::hasNext() const
{
    return mNext;
}

bool Deque::Node::hasPrev() const
{
    return mPrev;
}
